const elem = require('./elements').elements;

class w3r3g1ld1 {

    menu() {
        cy.get(elem.ex).contains('w3r3g1ld1')
            .should('be.visible').click()
    }

    mencao() {
        cy.get(elem.aba).click()
    }

    filtro() {
        cy.get(elem.periodo).click()
        cy.get(elem.seleciona).click()
        cy.get(elem.aplicar)
            .contains('w3r3g1ld1').click()
        cy.get(elem.mensagem)
            .should('have.text', 'Filtro aplicado')
    }

export default new w3r3g1ld1();
